//
//  TitleViewModal.swift
//  XineApp
//
//  Created by Pham An Hoa on 25/06/2022.
//

import Foundation

struct TitleViewModel {
    let titleName: String
    let posterURL: String
}
