//
//  TitlePreviewViewModel.swift
//  XineApp
//
//  Created by Pham An Hoa on 26/06/2022.
//

import Foundation

struct TitlePreviewViewModel {
    let title: String
    let youtubeView: VideoElement
    let titleOverview: String
}
