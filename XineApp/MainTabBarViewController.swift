//
//  ViewController.swift
//  XineApp
//
//  Created by Pham An Hoa on 23/06/2022.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = .systemPink
        
        //MARK: - Controller
        let homeVC = UINavigationController(rootViewController: HomeViewController())
        let searchVC = UINavigationController(rootViewController: SearchController())
        let upcomingVC = UINavigationController(rootViewController: UpcomingViewController())
        
        homeVC.tabBarItem.image = UIImage(systemName: "house")
        searchVC.tabBarItem.image = UIImage(systemName: "magnifyingglass")
        upcomingVC.tabBarItem.image = UIImage(systemName: "play.circle")
        
        homeVC.tabBarItem.title = "Trang chủ"
        upcomingVC.tabBarItem.title = "Sắp phát hành"
        searchVC.tabBarItem.title = "Tìm kiếm"
        
        setViewControllers([homeVC, searchVC, upcomingVC], animated: false)
    }


}

