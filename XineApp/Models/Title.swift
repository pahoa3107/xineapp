//
//  Movie.swift
//  XineApp
//
//  Created by Pham An Hoa on 25/06/2022.
//

import Foundation

//Codable là alias của Decodable và Encodable, thêm vào để có thể decode thành 1 format mà swift có thể hiểu được
struct TrendingTitleResponse: Codable {
    let results: [Title]
}

struct Title: Codable {
    let id: Int
    let media_type: String?
    let original_name: String?
    let original_title: String?
    let poster_path: String?
    let overview: String?
    let vote_count: Int
    let release_date: String?
    let vote_average: Double
}
