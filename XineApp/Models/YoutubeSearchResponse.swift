//
//  YoutubeSearchResponse.swift
//  XineApp
//
//  Created by Pham An Hoa on 26/06/2022.
//

import Foundation

struct YoutubeSearchResponse: Codable {
    let items: [VideoElement]
}

struct VideoElement: Codable {
    let id: IdVideoElement
}

struct IdVideoElement: Codable {
    let kind: String
    let videoId: String
}
