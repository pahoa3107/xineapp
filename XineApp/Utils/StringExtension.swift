//
//  String.swift
//  XineApp
//
//  Created by Pham An Hoa on 25/06/2022.
//

import Foundation

extension String {
    func capitalizeFirstChar() -> String {
        return self.prefix(1).uppercased() + self.lowercased().dropFirst()
    }
}
